﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Watcher
{
    public partial class Form1 : Form
    {
        string WatcherPath;
        string[] PreviousCheckFiles, CurrentCheckFiles, DifferenciesInFiles;
        string[] PreviousCheckDirs, CurrentCheckDirs, DifferenciesInDirs;
        public Form1()
        {
            InitializeComponent();
            timer1.Interval = 500;
            label1.Text = "Path";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WatcherPath = openFileDialog1.FileName;
                label1.Text = WatcherPath;
                textBox1.AppendText("Watching " + WatcherPath + "...\n");
                timer1.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            CheckForChanges(WatcherPath);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WatcherPath = folderBrowserDialog1.SelectedPath;
                label1.Text = WatcherPath;
                textBox1.AppendText("Watching " + WatcherPath + "...\n");
                PreviousCheckFiles = Directory.GetFiles(WatcherPath);
                PreviousCheckDirs = Directory.GetDirectories(WatcherPath);
                timer1.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            label1.Text = "Path";
            textBox1.AppendText("Watching ended... Path reseted...\n");
        }

        public void CheckForChanges(string p)
        {
            #region Проверяем изменение файлов
            CurrentCheckFiles = Directory.GetFiles(p);
            if (!ArraysEqual(PreviousCheckFiles, CurrentCheckFiles))
            {
                textBox1.AppendText("Files are changed! ");
                if (PreviousCheckFiles.Length < CurrentCheckFiles.Length)
                {
                    DifferenciesInFiles = CurrentCheckFiles.Except(PreviousCheckFiles).ToArray();
                    textBox1.AppendText("Added:");
                    foreach (string n in DifferenciesInFiles)
                    {
                        textBox1.AppendText(" " + n.Replace(p + "\\", ""));
                    }
                    textBox1.AppendText("   " + DateTime.Now.ToString() + "\n");
                }
                else if (PreviousCheckFiles.Length > CurrentCheckFiles.Length)
                {
                    DifferenciesInFiles = PreviousCheckFiles.Except(CurrentCheckFiles).ToArray();
                    textBox1.AppendText("Deleted: ");
                    foreach (string n in DifferenciesInFiles)
                    {
                        textBox1.AppendText(" " + n.Replace(p + "\\", ""));
                    }
                    textBox1.AppendText("   " + DateTime.Now.ToString() + "\n");
                }
                else if (PreviousCheckFiles.Length == CurrentCheckFiles.Length)
                {
                    DifferenciesInFiles = CurrentCheckFiles.Except(PreviousCheckFiles).ToArray();
                    textBox1.AppendText("Renamed: ");
                    foreach (string n in DifferenciesInFiles)
                    {
                        textBox1.AppendText(" " + n.Replace(p+"\\", ""));
                    }
                    textBox1.AppendText("   " + DateTime.Now.ToString() + "\n");
                }
                PreviousCheckFiles = CurrentCheckFiles;
            }
            #endregion

            #region Проверяем изменение папок
            CurrentCheckDirs = Directory.GetDirectories(p);
            if (!ArraysEqual(PreviousCheckDirs, CurrentCheckDirs))
            {
                textBox1.AppendText("Directories are changed! ");
                if (PreviousCheckDirs.Length < CurrentCheckDirs.Length)
                {
                    DifferenciesInDirs = CurrentCheckDirs.Except(PreviousCheckDirs).ToArray();
                    textBox1.AppendText("Added:");
                    foreach (string n in DifferenciesInDirs)
                    {
                        textBox1.AppendText(" " + n.Replace(p + "\\", ""));
                    }
                    textBox1.AppendText("   " + DateTime.Now.ToString() + "\n");
                }
                else if (PreviousCheckDirs.Length > CurrentCheckDirs.Length)
                {
                    DifferenciesInDirs = PreviousCheckDirs.Except(CurrentCheckDirs).ToArray();
                    textBox1.AppendText("Deleted: ");
                    foreach (string n in DifferenciesInDirs)
                    {
                        textBox1.AppendText(" " + n.Replace(p + "\\", ""));
                    }
                    textBox1.AppendText("   " + DateTime.Now.ToString() + "\n");
                }
                else if (PreviousCheckDirs.Length == CurrentCheckDirs.Length)
                {
                    DifferenciesInDirs = CurrentCheckDirs.Except(PreviousCheckDirs).ToArray();
                    textBox1.AppendText("Renamed: ");
                    foreach (string n in DifferenciesInDirs)
                    {
                        textBox1.AppendText(" " + n.Replace(p + "\\", ""));
                    }
                    textBox1.AppendText("   " + DateTime.Now.ToString() + "\n");
                }
                PreviousCheckDirs = CurrentCheckDirs;
            }
            #endregion
        }

        static bool ArraysEqual(string[] a1, string[] a2)
        {
            if (a1.Length == a2.Length)
            {
                for (int i = 0; i < a1.Length; i++)
                {
                    if (!Equals(a1[i], a2[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}
