﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Set
{
    public class Set<T> : IEnumerable<T>
    {

        private List<T> _items = new List<T>();

        public int Count => _items.Count;
        //Добавление элемента
        public void Add(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (!_items.Contains(item))
            {
                _items.Add(item);
            }
        }
        //Удаление элемента
        public void Remove(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (!_items.Contains(item))
            {
                throw new KeyNotFoundException($"Элемент {item} не найден в множестве.");
            }

            _items.Remove(item);
        }
        //Сложение
        public static Set<T> operator +(Set<T> set1, Set<T> set2)
        {
            if (set1 == null)
            {
                throw new ArgumentNullException(nameof(set1));
            }

            if (set2 == null)
            {
                throw new ArgumentNullException(nameof(set2));
            }

            var resultSet = new Set<T>();

            var items = new List<T>();

            if (set1._items != null && set1._items.Count > 0)
            {
                items.AddRange(new List<T>(set1._items));
            }

            if (set2._items != null && set2._items.Count > 0)
            {
                items.AddRange(new List<T>(set2._items));
            }

            resultSet._items = items.Distinct().ToList();

            return resultSet;
        }
        //Вычитание
        public static Set<T> operator -(Set<T> set1, Set<T> set2)
        {
            if (set1 == null)
            {
                throw new ArgumentNullException(nameof(set1));
            }

            if (set2 == null)
            {
                throw new ArgumentNullException(nameof(set2));
            }

            var resultSet = new Set<T>();

            var items = new List<T>();

            foreach (var item in set1._items)
            {
                if (!set2._items.Contains(item))
                {
                    resultSet.Add(item);
                }
            }

            return resultSet;
        }
        //Пересечение
        public static Set<T> Intersection(Set<T> set1, Set<T> set2)
        {
            if (set1 == null)
            {
                throw new ArgumentNullException(nameof(set1));
            }

            if (set2 == null)
            {
                throw new ArgumentNullException(nameof(set2));
            }

            var resultSet = new Set<T>();

            if (set1.Count < set2.Count)
            {
                foreach (var item in set1._items)
                {
                    if (set2._items.Contains(item))
                    {
                        resultSet.Add(item);
                    }
                }
            }
            else
            {
                foreach (var item in set2._items)
                {
                    if (set1._items.Contains(item))
                    {
                        resultSet.Add(item);
                    }
                }
            }

            return resultSet;
        }
        //Разница
        public static Set<T> Difference(Set<T> set1, Set<T> set2)
        {
            if (set1 == null)
            {
                throw new ArgumentNullException(nameof(set1));
            }

            if (set2 == null)
            {
                throw new ArgumentNullException(nameof(set2));
            }

            var resultSet = new Set<T>();

            foreach (var item in set1._items)
            {
                if (!set2._items.Contains(item))
                {
                    resultSet.Add(item);
                }
            }

            foreach (var item in set2._items)
            {
                if (!set1._items.Contains(item))
                {
                    resultSet.Add(item);
                }
            }

            resultSet._items = resultSet._items.Distinct().ToList();

            return resultSet;
        }
        //Проверка на подмножество
        public static bool Subset(Set<T> set1, Set<T> set2)
        {
            if (set1 == null)
            {
                throw new ArgumentNullException(nameof(set1));
            }

            if (set2 == null)
            {
                throw new ArgumentNullException(nameof(set2));
            }

            var result = set1._items.All(s => set2._items.Contains(s));
            return result;
        }

        /*public static bool operator ==(Set<T> set1, Set<T> set2)
        {
            if ((Object)set1 == null || (Object)set2 == null)
                return false;

            var result1 = set1._items.All(s => set2._items.Contains(s));
            var result2 = set2._items.All(s => set1._items.Contains(s));
            if (result1 && result2)
                return true;
            else
                return false;
        }

        public static bool operator !=(Set<T> set1, Set<T> set2)
        {
            if ((Object)set1 != null && (Object)set2 != null)
                return false;

            var result1 = set1._items.All(s => set2._items.Contains(s));
            var result2 = set2._items.All(s => set1._items.Contains(s));

            if (result1)
                return result1;
            return !(result1 == result2);
        }*/
        public bool Equals(Set<T> other)
        {
            return (!ReferenceEquals(null, other) && ReferenceEquals(this, other) || (_items.All(s => other._items.Contains(s))) && (other._items.All(s => _items.Contains(s))));
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Set<T>);
        }

        public static bool operator ==(Set<T> left, Set<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Set<T> left, Set<T> right)
        {
            return !Equals(left, right);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Создаем множества.
            var set1 = new Set<int>()
            {
                1, 2, 3, 4, 5, 6
            };

            var set2 = new Set<int>()
            {
                4, 5, 6, 7, 8, 9
            };

            var set3 = new Set<int>()
            {
                2, 3, 4
            };
            var set4 = new Set<int>()
            {
                2, 3, 5
            };

            // Выполняем операции со множествами.
            var union = set1 + set2;
            var decrease = set1 - set2;
            var difference = Set<int>.Difference(set1, set2);
            var intersection = Set<int>.Intersection(set1, set2);
            var subset1 = Set<int>.Subset(set3, set1);
            var subset2 = Set<int>.Subset(set3, set2);
            var diff1 = set3 == set4;
            var diff2 = set3 != set4;

            PrintSet(set1, "Первое множество: ");
            PrintSet(set2, "Второе множество: ");
            PrintSet(set3, "Третье множество: ");
            PrintSet(set4, "Четвертое множество: ");

            PrintSet(union, "Объединение первого и второго множества (+): ");
            PrintSet(decrease, "Вычитание второго множества из первого (-): ");
            PrintSet(difference, "Разность первого и второго множества: ");
            PrintSet(intersection, "Пересечение первого и второго множества: ");
            if (diff1)
            {
                Console.WriteLine("Третье и четвертое множества совпадают.");
            }
            else
            {
                Console.WriteLine("Третье и четвертое множества не совпадают.");
            }

            if (diff2)
            {
                Console.WriteLine("Третье и четвертое множества отличаются.");
            }
            else
            {
                Console.WriteLine("Третье и четвертое множества не отличаются.");
            }


            if (subset1)
            {
                Console.WriteLine("Третье множество является подмножеством первого.");
            }
            else
            {
                Console.WriteLine("Третье множество не является подмножеством первого.");
            }

            if (subset2)
            {
                Console.WriteLine("Третье множество является подмножеством второго.");
            }
            else
            {
                Console.WriteLine("Третье множество не является подмножеством второго.");
            }

            Console.ReadLine();
        }

        private static void PrintSet(Set<int> set, string title)
        {
            Console.Write(title);
            foreach (var item in set)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
        }
    }

}