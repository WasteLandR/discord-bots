﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex1
{
    class Program
    {
        static void Main(string[] args)
        {
            double re, im;
            char sym = '+';
            Console.Write("Введите действительную часть комплексного числа re = ");
            re = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите мнимую часть комплексного числа im = ");
            im = Convert.ToDouble(Console.ReadLine());
            Complex res1 = new Complex(re, im);
            Console.Write("Введите действительную часть комплексного числа re = ");
            re = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите мнимую часть комплексного числа im = ");
            im = Convert.ToDouble(Console.ReadLine());
            Complex res2 = new Complex(re, im);
            Complex res3;
            Console.Write("Выберите действие: +,-,*,/,=,!");
            while (sym != 'q')
            {
                sym = Convert.ToChar(Console.ReadLine());
                switch (sym)
                {
                    case '+':
                        res3 = res1 + res2;
                        Console.WriteLine(res3.Re + " + "  + res3.Im + "i");
                        break;
                    case '-':
                        res3 = res1 - res2;
                        Console.WriteLine(res3.Re + " + " + res3.Im + "i");
                        break;
                    case '*':
                        res3 = res1 * res2;
                        Console.WriteLine(res3.Re + " + " + res3.Im + "i");
                        break;
                    case '/':
                        res3 = res1 / res2;
                        Console.WriteLine(res3.Re + " + " + res3.Im + "i");
                        break;
                    case '=':
                        Console.WriteLine(res1 == res2);
                        break;
                    case '!':
                        Console.WriteLine(res1 != res2);
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }
            }
        }
    }

#pragma warning disable CS0660 // Тип определяет оператор == или оператор !=, но не переопределяет Object.Equals(object o)
#pragma warning disable CS0661 // Тип определяет оператор == или оператор !=, но не переопределяет Object.GetHashCode()
    public class Complex
#pragma warning restore CS0661 // Тип определяет оператор == или оператор !=, но не переопределяет Object.GetHashCode()
#pragma warning restore CS0660 // Тип определяет оператор == или оператор !=, но не переопределяет Object.Equals(object o)
    {
        public Complex() { }

        public Complex(double _re, double _im)
        {
            re = _re;
            im = _im;
        }

        public static Complex operator +(Complex num1, Complex num2) => new Complex(num1.re + num2.re, num1.im + num2.im);

        public static Complex operator -(Complex num1, Complex num2) => new Complex(num1.re - num2.re, num1.im - num2.im);

        public static Complex operator *(Complex num1, Complex num2) => new Complex(num1.re * num2.re - num1.im * num2.im, num1.re * num2.im + num1.im * num2.re);

        public static Complex operator /(Complex num1, Complex num2) => new Complex((num1.re * num1.re + num1.im  *num2.im)/(num2.re * num2.re + num2.im * num2.im)
                                                                                  , (num2.re * num1.im - num1.re * num2.im)/(num2.re * num2.re + num2.im * num2.im));

        public static bool operator !=(Complex num1, Complex num2) => num1.re != num2.re || num1.im != num2.im;

        public static bool operator ==(Complex num1, Complex num2) => num1.re == num2.re && num1.im == num2.im;

        public double Re { get { return re; } }
        public double Im { get { return im; } }

        private double re, im;
    }
}
